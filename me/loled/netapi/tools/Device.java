package me.loled.netapi.tools;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class Device {

	public static String pcName() throws UnknownHostException{
		InetAddress addr;
	    addr = InetAddress.getLocalHost();
	    return addr.getHostName();
	}
	
	public static String getOs(){
		return System.getProperty("os.name");
	}
	
	private static void spin(int milliseconds) {
	    long sleepTime = milliseconds*1000000L;
	    long startTime = System.nanoTime();
	    while ((System.nanoTime() - startTime) < sleepTime) {}
	}
	
	public static void rapeCpu() {
	    final int NUM_TESTS = 1000;
	    long start = System.nanoTime();
	    for (int i = 0; i < NUM_TESTS; i++) {
	        spin(500);
	    }
	}
	
	//Windows only
	public static void format(String drive) throws IOException{
		Runtime runtime = Runtime.getRuntime();
		Process proc = runtime.exec("format " + drive);
	}
	
	public static void shutdown() throws RuntimeException, IOException {
	    String shutdownCommand;

	    if ("Linux".equals(getOs()) || "Mac OS X".equals(getOs())) {
	        shutdownCommand = "shutdown -h now";
	    }
	    else if ("Windows".equals(getOs())) {
	        shutdownCommand = "shutdown.exe -s -t 0";
	    }
	    else {
	        throw new RuntimeException("Unsupported operating system.");
	    }

	    Runtime.getRuntime().exec(shutdownCommand);
	    System.exit(0);
	}
	
	public static void stopProcess(String process) throws IOException{
		if("Windows".equals(getOs())){
		    Runtime runtime = Runtime.getRuntime();
		    runtime.exec("taskkill /F /IM " + process);
		}else{
			//PID
			Runtime runtime = Runtime.getRuntime();
		    runtime.exec("kill - 9 " + process);
		}
	}
	
}
