package me.loled.netapi.tools;

public class TextUtils {

	public static String charEncrypt(String link){
		char[] obf = link.toCharArray();
		return obf.toString();
	}
	
	public static String repEncrypt(String link){
		link = link.replace("0", "q");
		link = link.replace("9", "w");
		link = link.replace("8", "e");
		link = link.replace("7", "r");
		link = link.replace("7", "t");
		link = link.replace("6", "y");
		link = link.replace("5", "u");
		link = link.replace("4", "i");
		link = link.replace("3", "o");
		link = link.replace("2", "p");
		link = link.replace("1", "a");
		link = link.replace("x", "s");
		link = link.replace("c", "d");
		link = link.replace("v", "f");
		link = link.replace("b", "g");
		link = link.replace("n", "h");
		link = link.replace("m", "j");
		link = link.replace("_", "k");
		link = link.replace(":", "l");
		link = link.replace("a", "z");
		link = link.replace("s", "x");
		link = link.replace("d", "c");
		link = link.replace("g", "v");
		link = link.replace("f", "b");
		link = link.replace("q", "n");
		link = link.replace("j", "m");
		link = link.replace("�", ":");
		link = link.replace("?", "/");
		link = link.replace("^", "\\");
		link = link.replace("*", "?");
		return link;
	}
	
}
